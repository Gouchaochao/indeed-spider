FROM python:3.7-slim
ENV app /data
WORKDIR $app
ENV PORT 5000
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .
ENTRYPOINT ["python", "main.py"]