import requests
from lxml import etree
from faker import Faker

fk = Faker()


class InertJobLinksToFile(object):
    """
    """
    def __init__(self, search_title, file_name):
        self.basic_url = "https://www.indeed.com"
        self.search_title = search_title
        self.file_name = file_name

    def get_links(self, offset_number=0):
        url_request_param = {
            "q": f"title%3A%22{self.search_title}%22",
            "l": "US",
            "sort": "date",
            "limit": 50,
            "fromage": 1,
            "start": offset_number
        }
        cards_response = requests.get(
            "https://www.indeed.com/jobs",
            params=url_request_param,
            headers={"user-agent": fk.user_agent()}
        )
        cards_page_content = etree.HTML(cards_response.text)
        apply_links = cards_page_content.xpath('//a[@class="jcs-JobTitle"]')
        ulr_list = [self.basic_url + al.get("href") + "\n" for al in apply_links]
        with open(self.file_name, "a+") as f:
            f.write("".join(ulr_list))
        if len(ulr_list) <= 50:
            return
        offset_number += 50
        return self.get_links(offset_number)


if __name__ == "__main__":
    InertJobLinksToFile("machine+learning+engineer", "mle.dat").get_links()